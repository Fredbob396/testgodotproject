using Godot;
using TestGodotProject.classes;

public class TextChar : Sprite
{
    public int Char;

    public void SetChar(char c)
    {
        Char = c;
        int? frame = GetFrame(c);
        if(frame != null)
            SetFrame((int)frame);
        else
            SetFrame(92); // Space
    }

    private int? GetFrame(char c)
    {
        int? frame;
        GConstants.CharIntegerDictionary.TryGetValue(c, out frame);
        return frame;
    }

    //public override void _Process(float delta)
    //{
    //    // Called every frame. Delta is time since last frame.
    //    // Update game logic here.
    //}
}
