extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var timer

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	timer = Timer.new()
	timer.connect("timeout", self, "tick")
	add_child(timer)
	timer.wait_time = 0.2
	timer.start()

func tick():
	self.frame += 1;

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
