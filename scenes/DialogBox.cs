using System.Collections.Generic;
using System.Linq;
using Godot;
using System.Threading.Tasks;
using TestGodotProject.classes;

public class DialogBox : Node2D
{
    private Node _textRoot;
    private Sprite _portrait;
    private List<AudioStreamPlayer2D> _voices;

    private const int XIndent = 15;
    private const int XStep = 8;
    private const int YStep = 18;

    private const float BaseDelay = 0.03f;
    private const float BasePunctuationDelayFactor = 5.33f;

    private readonly char[] _punctuationDelayChars = { ',', ';', '!', '?' };
    private readonly char[] _silentChars = { '*', ' ', ',', ';', '!', '?' };

    private PackedScene _txtCharScene;

    private bool _skipMessage = false;

    public override void _Ready()
    {
        _txtCharScene = (PackedScene)ResourceLoader.Load("res://scenes/TextChar.tscn");
        _textRoot = FindNode("TextRoot");
        _portrait = (Sprite)FindNode("Portrait");

        _voices = new List<AudioStreamPlayer2D>();
        foreach (AudioStreamPlayer2D voice in FindNode("Voices").GetChildren())
            _voices.Add(voice);

        PlayDialogAsync();
    }

    public override void _Process(float delta)
    {
        if (Input.IsActionPressed("player_speedup") && _skipMessage == false)
            _skipMessage = true;
    }

    private async void PlayDialogAsync()
    {
        await Task.Run(PlayDialog);
        SetProcess(false);
        Hide();
    }

    /// <summary>
    /// Play all dialog messages
    /// </summary>
    private async Task<bool> PlayDialog()
    {
        // TODO: Have dialog get set elsewhere or something. Like it is in the Unity project
        var db = new DialogBuilder(Speakers.Ralsei);
        db.Add("Oh, you must be Kris!* One of the heroes of legend!");
        db.Add("Well...* Shut. The Fuck. UPPP!* I don't give a fuck!", Expressions.RalseiAngry);
        db.Add("Oi there.* New fuckin' line, innit.", Expressions.RalseiUnamused);
        db.Add("Wew, lad!", Expressions.RalseiEyesClosedSmiling);
        db.Add("A!*B...*C?*D.", Expressions.RalseiConfused);

        foreach (DialogMessage message in db.GetDialog())
        {
            bool messagedone = false;
            _skipMessage = false;

            // Play the message synchronously
            PlayMessage(message);
            // Await user input before moving to next message
            while (!messagedone)
            {
                messagedone = Input.IsActionJustPressed("player_activate");
            }

            // Free all old text nodes before printing next message
            foreach (Node child in _textRoot.GetChildren())
            {
                child.Free();
            }
        }
        return true;
    }

    /// <summary>
    /// Play a single message synchronously
    /// </summary>
    private void PlayMessage(DialogMessage message)
    {
        // Set portrait
        _portrait.Frame = message.Expression;

        // Get intial char offset
        var charOffset = new Vector2(XIndent, 0);

        foreach (DialogLine dialogLine in message.Text.Lines)
        {
            char[] line = dialogLine.GetLine();

            // If newline, put asterix
            if (dialogLine.IsNewline)
            {
                var astxChar = (TextChar)_txtCharScene.Instance();
                astxChar.SetChar('*');

                // Put asterix at special '0' position
                Vector2 astxCharPos = astxChar.Position;
                astxCharPos.x = 0;
                astxCharPos.y = charOffset.y;
                astxChar.Position = astxCharPos;

                _textRoot.AddChild(astxChar);
            }

            var periodDelayIndexes = GetPeriodDelayIndexes(line);

            // For each character in line, print it out one by one
            for (int index = 0; index < line.Length; index++)
            {
                char c = line[index];

                // Ignore empty chars completely
                if (c == '\0')
                    continue;

                float punctuationDelayFactor = 1.0f;

                // If not a space, print it
                if (c != ' ')
                {
                    // Apply text delay based on punctuation characters
                    if (_punctuationDelayChars.Contains(c) || periodDelayIndexes.Contains(index))
                        punctuationDelayFactor += BasePunctuationDelayFactor;

                    var txtChar = (TextChar)_txtCharScene.Instance();
                    txtChar.SetChar(c);

                    // Put char at tracked coordinates
                    Vector2 txtCharPos = txtChar.Position;
                    txtCharPos.x = charOffset.x;
                    txtCharPos.y = charOffset.y;
                    txtChar.Position = txtCharPos;

                    _textRoot.AddChild(txtChar);

                    // Don't play sound when skipping or silent char
                    if (!_skipMessage && !_silentChars.Contains(c))
                        PlayVoice();
                }
                // Move to next character
                charOffset.x += XStep;

                // Sleep based on how much the character is set to be delayed
                if (!_skipMessage)
                    Sleeper.DelaySeconds(BaseDelay * punctuationDelayFactor);
            }
            // Reset x and move to next line
            charOffset.x = XIndent;
            charOffset.y += YStep;
        }
    }

    /// <summary>
    /// Get the indexes of single characters or ends of ellipsis for applying delays
    /// </summary>
    int[] GetPeriodDelayIndexes(char[] message)
    {
        // Get all period indexes
        var periodIndexes = Enumerable.Range(0, message.Length)
            .Where(i => message[i] == '.')
            .ToList();

        var delayIndexes = new List<int>();

        foreach (int periodIndex in periodIndexes)
        {
            var lCheck = periodIndex - 1;
            var rCheck = periodIndex + 1;

            // Get neighbors if not out of bounds, else get space
            var leftNeighbor = lCheck >= 0
                ? message[lCheck]
                : ' ';

            var rightNeighbor = rCheck < message.Length
                ? message[rCheck]
                : ' ';

            // Single period
            if (leftNeighbor != '.' && rightNeighbor != '.')
                delayIndexes.Add(periodIndex);

            // End of ellipsis
            if (leftNeighbor == '.' && rightNeighbor != '.')
                delayIndexes.Add(periodIndex);
        }
        return delayIndexes.ToArray();
    }

    /// <summary>
    /// Plays voice with allowed polyphony. 
    /// Annoying workaround because GD3 doesn't support polyphony from new audio sources yet.
    /// </summary>
    private void PlayVoice()
    {
        var availableVoice = _voices.FirstOrDefault(x => !x.Playing);
        if (availableVoice != null)
        {
            availableVoice.Play();
        }
        else
        {
            GD.PrintErr("No available voice for DialogBox!");
        }
    }
}
