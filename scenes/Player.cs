using Godot;
using System;

public class Player : KinematicBody2D
{
    // Node stuff
    private AnimatedSprite _anim;
    private SpriteFrames _sFrames;

    // Movement speed constants
    private const float Speed = 1f;
    private const float RunSpeedFactor = 1.5f;
    private const float BaseWalkAnimSpeed = 4.0f; 

    // TODO: Separate class?
    // Animation constants
    private const string AnimIdleUp = "idle_up";
    private const string AnimIdleDown = "idle_down";
    private const string AnimIdleLeft = "idle_left";
    private const string AnimIdleRight = "idle_right";
    private const string AnimWalkUp = "walk_up";
    private const string AnimWalkDown = "walk_down";
    private const string AnimWalkLeft = "walk_left";
    private const string AnimWalkRight = "walk_right";

    // Tracking variables
    private Vector2 _prevVelocity;
    private bool _isRunning;

    public override void _Ready()
    {
        _anim = GetNode<AnimatedSprite>("RalseiAnimated");
        _sFrames = _anim.GetSpriteFrames();
    }
    
    public override void _PhysicsProcess(float delta)
    {
        var velocity = new Vector2();
        float speed;

        // TODO: Use proper input stuff?
        if (Input.IsKeyPressed((int) KeyList.X))
        {
            speed = Speed * RunSpeedFactor;
            _isRunning = true;
        }
        else
        {
            speed = Speed;
            _isRunning = false;
        }

        // Move left if not moved right previous frame
        if (Input.IsKeyPressed((int)KeyList.Left) && _prevVelocity.x <= 0)
            velocity.x -= speed;

        // Move right if not moved left previous frame
        if (Input.IsKeyPressed((int)KeyList.Right) && _prevVelocity.x >= 0)
            velocity.x += speed;

        // Move up if not moved down previous frame
        if (Input.IsKeyPressed((int)KeyList.Up) && _prevVelocity.y <= 0)
            velocity.y -= speed;

        // Move down in not moved up previous frame
        if (Input.IsKeyPressed((int)KeyList.Down) && _prevVelocity.y >= 0)
            velocity.y += speed;

        // Move using kinematics
        MoveAndCollide(velocity);

        ManageAnimation(velocity.x, velocity.y);
        _prevVelocity = velocity;
    }

    void ManageAnimation(float x, float y)
    {
        // Handle Stopping
        if (x == 0 && y == 0)
        {
            switch (_anim.Animation)
            {
                case AnimWalkLeft:
                    _anim.Animation = AnimIdleLeft;
                    break;
                case AnimWalkRight:
                    _anim.Animation = AnimIdleRight;
                    break;
                case AnimWalkUp:
                    _anim.Animation = AnimIdleUp;
                    break;
                case AnimWalkDown:
                    _anim.Animation = AnimIdleDown;
                    break;
            }
        }
        // Handle Changing Direction
        else
        {
            // If there's no horizontal movement
            if (x == 0)
            {
                // There must be vertical movement, so walk up or down depending on y
                if (y < 0)
                    _anim.Animation = AnimWalkUp;
                else if (y > 0)
                    _anim.Animation = AnimWalkDown;
            }
            // If there's no vertical movement
            if (y == 0)
            {
                // There must be horizontal movement, so walk left or right depending on x
                if (x > 0)
                    _anim.Animation = AnimWalkRight;
                else if (x < 0)
                    _anim.Animation = AnimWalkLeft;
            }

            // If there is movement on both axes and the character is not walking horizontally
            if ((Math.Abs(y) > 0 && Math.Abs(x) > 0) && !(_anim.Animation.Equals(AnimWalkLeft) || _anim.Animation.Equals(AnimWalkRight)))
            {
                // Use the walk up or down animation for diagonal movement
                if (y < 0)
                    _anim.Animation = AnimWalkUp;
                else if (y > 0)
                    _anim.Animation = AnimWalkDown;
            }
        }
        // Set Animation Speed
        if (_isRunning)
            _sFrames.SetAnimationSpeed(_anim.Animation, BaseWalkAnimSpeed * RunSpeedFactor);
        else
            _sFrames.SetAnimationSpeed(_anim.Animation, BaseWalkAnimSpeed);
    }
}
