using Godot;

public class ScreenScale : Node
{
    [Export]
    public int ViewportScaleFactor;

    public override void _Ready()
    {
        var viewPort = GetViewport();

        // Original resolution
        float viewportWidth = viewPort.Size.x;
        float viewportHeight = viewPort.Size.y;

        // Upscaled resolution
        float upscaledViewportWidth = viewportWidth * ViewportScaleFactor;
        float upscaledViewportHeight = viewportHeight * ViewportScaleFactor;

        GD.Print($"Original Resolution: {viewportWidth}x{viewportHeight}");
        GD.Print($"Upscaled Resolution: {upscaledViewportWidth}x{upscaledViewportHeight}");

        // Apply upscaled resolution
        OS.WindowSize = new Vector2(upscaledViewportWidth, upscaledViewportHeight);
        //OS.WindowFullscreen = true;
    }
}
