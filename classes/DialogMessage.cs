﻿namespace TestGodotProject.classes
{
    class DialogMessage
    {
        public DialogText Text { get; set; }
        public DialogSpeaker Speaker { get; set; }
        public int Expression { get; set; }
        public float Delay { get; set; }
        public bool AutoSkip { get; set; }
        public bool NoPunctuationDelay { get; set; }
    }
}
