﻿namespace TestGodotProject.classes
{
    class Speakers
    {
        public static DialogSpeaker Ralsei = new DialogSpeaker
        {
            Id = "ralsei",
            Portrait = "ralsei_portraits",
            Sound = "snd_txtral",
            Font = "Determination Mono"
        };
        public static DialogSpeaker Lancer = new DialogSpeaker
        {
            Id = "lancer",
            Portrait = "lancer_portraits",
            Sound = "lancer_sound",
            Font = "Determination Mono"
        };
        public static DialogSpeaker None = new DialogSpeaker
        {
            Id = "none",
            Sound = "text_sound",
            Font = "Determination Mono"
        };
    }
}
