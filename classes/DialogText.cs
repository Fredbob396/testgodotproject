﻿using System.Collections.Generic;
using System.Linq;

namespace TestGodotProject.classes
{
    class DialogText
    {
        public List<DialogLine> Lines = new List<DialogLine>();
        private int _lineIndex;

        private readonly char[] _wholeWordEndings = {' ', '*'};

        /// <summary>
        /// Builds a list of properly formatted DialogLines for dialog boxes 
        /// </summary>
        /// <param name="message">The message to build a DialogText object from</param>
        /// <param name="lineCount">The number of lines to build a DialogText from</param>
        /// <param name="lineCharacters">The number of characters per-line</param>
        public DialogText(string message = null, int lineCount = 3, int lineCharacters = 24)
        {
            if (message != null)
            {
                BuildDialogText(message, lineCount, lineCharacters);
            }
        }

        /// <summary>
        /// Builds a list of properly formatted DialogLines
        /// </summary>
        /// <param name="message">The message to build a DialogText object from</param>
        /// <param name="lineCount">The number of lines to build a DialogText from</param>
        /// <param name="lineCharacters">The number of characters per-line</param>
        public void BuildDialogText(string message, int lineCount = 3, int lineCharacters = 24)
        {
            SetLines(lineCount, lineCharacters);

            bool doneProcessing = false;
            var line = GetLine();
            line.IsNewline = true;
            int mIdx = 0;

            while (!doneProcessing)
            {
                bool isSingleChar = false;
                string wholeWord = "";

                // End of message
                if (mIdx == message.Length)
                {
                    doneProcessing = true;
                    continue;
                }

                // Try to get new line if current is full, else done processing
                if (line.IsFull())
                {
                    line = GetNextLine();
                    if (line == null)
                    {
                        doneProcessing = true;
                        continue;
                    }
                }

                // Get the current tracked char
                char c = message[mIdx];

                // If char is a space and not empty, put a space
                if (c == ' ')
                {
                    isSingleChar = true;
                    if (!line.IsEmpty())
                        line.TryPutChar(c);
                }
                // If asterisk, end line early.
                else if (c == '*')
                {
                    line = GetNextLine();
                    if (line == null)
                    {
                        // No more lines!
                        doneProcessing = true;
                        continue;
                    }
                    line.IsNewline = true;
                    isSingleChar = true;
                }
                // Else, char is the start of a new word
                else
                {
                    wholeWord = GetWholeWord(message, mIdx);
                    bool putSuccess = line.TryPutWholeWord(wholeWord);
                    if (!putSuccess)
                    {
                        // Get next line and try again
                        line = GetNextLine();
                        if (line == null)
                        {
                            // No more lines!
                            doneProcessing = true;
                            continue;
                        }
                        // Try one last time. If failure, give up. Word is bigger than a line!
                        putSuccess = line.TryPutWholeWord(wholeWord);
                        if (!putSuccess)
                        {
                            // TODO: Warning?
                            doneProcessing = true;
                            continue;
                        }
                    }
                }

                // Increment tracking index
                mIdx = isSingleChar
                    ? mIdx + 1
                    : mIdx + wholeWord.Length;
            }
        }

        /// <summary>
        /// (Re)sets the DialogText's DialogLines
        /// </summary>
        private void SetLines(int lineCount, int lineCharacters)
        {
            Lines.Clear();
            for (int i = 0; i < lineCount; i++)
            {
                Lines.Add(new DialogLine(lineCharacters));
            }
            _lineIndex = 0;
        }

        /// <summary>
        /// Get the line being currently processed
        /// </summary>
        private DialogLine GetLine()
        {
            return Lines[_lineIndex];
        }

        /// <summary>
        /// Increment the line index and get the next line if applicable
        /// </summary>
        private DialogLine GetNextLine()
        {
            _lineIndex++;
            return _lineIndex > Lines.Count - 1
                ? null
                : Lines[_lineIndex];
        }

        /// <summary>
        /// Gets a whole word given a string and a starting index (Includes punctuation)
        /// </summary>
        private string GetWholeWord(string message, int index)
        {
            string wholeWord = "";
            bool done = false;
            while (!done)
            {
                // End of message
                if (index == message.Length)
                {
                    done = true;
                    continue;
                }
                char c = message[index];
                // End of word
                if (_wholeWordEndings.Contains(c))
                {
                    done = true;
                    continue;
                }
                wholeWord += c;
                index++;
            }
            return wholeWord;
        }
    }
}
