﻿using System.Collections.Generic;

namespace TestGodotProject.classes
{
    class DialogBuilder
    {
        private readonly List<DialogMessage> _dialog;
        private DialogSpeaker _speaker;
        private int? _expression;
        private float? _delay;
        private bool? _autoSkip;
        private bool? _noPunctuationDelay;

        // TODO: Get defaults from a text file?
        public DialogBuilder(
            DialogSpeaker speaker = null,
            int expression = 0,
            float delay = 0.03f,
            bool autoSkip = false,
            bool noPunctuationDelay = false)
        {
            _dialog = new List<DialogMessage>();
            _speaker = speaker;
            _expression = expression;
            _delay = delay;
            _autoSkip = autoSkip;
            _noPunctuationDelay = noPunctuationDelay;
        }

        public List<DialogMessage> GetDialog()
        {
            return _dialog;
        }

        /// <summary>
        /// Pushes a line of dialog to the dialog list
        /// </summary>
        public void Add(string text, int? expression = null, DialogSpeaker speaker = null, float? delay = null, bool? autoSkip = null, bool? noPunctuationDelay = null)
        {
            ParseParams(ref speaker, ref expression, ref delay, ref autoSkip, ref noPunctuationDelay);
            _dialog.Add(new DialogMessage
            {
                Text = new DialogText(text),
                Speaker = speaker,
                Expression = (int)expression,
                Delay = (float)delay,
                AutoSkip = (bool)autoSkip,
                NoPunctuationDelay = (bool)noPunctuationDelay
            });
        }

        /// <summary>
        /// Clears the dialog list and reset dialog properties
        /// </summary>
        public void Clear()
        {
            _speaker = null;
            _expression = 0;
            _delay = 0.03f;
            _autoSkip = false;
            _noPunctuationDelay = false;
            _dialog.Clear();
        }

        private void ParseParams(ref DialogSpeaker speaker, ref int? expression, ref float? delay, ref bool? autoSkip, ref bool? noPunctuationDelay)
        {
            // Speaker
            if (speaker == null)
                speaker = _speaker;
            else
                _speaker = speaker;

            // Expression
            if (expression == null)
                expression = _expression;
            else
                _expression = expression;

            // Delay
            if (delay == null)
                delay = _delay;
            else
                _delay = delay;

            // Auto Skip
            if (autoSkip == null)
                autoSkip = _autoSkip;
            else
                _autoSkip = (bool)autoSkip;

            // No Punctuation Delay
            if (noPunctuationDelay == null)
                noPunctuationDelay = _noPunctuationDelay;
            else
                _noPunctuationDelay = (bool)noPunctuationDelay;
        }
    }
}
