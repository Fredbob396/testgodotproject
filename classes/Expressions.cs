﻿namespace TestGodotProject.classes
{
    /// <summary>
    /// Dialog Expression Constants
    /// </summary>
    class Expressions
    {
        public const int RalseiSmiling = 0;
        public const int RalseiLookingDown = 1;
        public const int RalseiLookingDownBlushing = 2;
        public const int RalseiShocked = 3;
        public const int RalseiLookingDownConcerned = 4;
        public const int RalseiLookingDownConcernedSmile = 5;
        public const int RalseiConcernedSmile = 6;
        public const int RalseiConcernedSmileBlushing = 7;
        public const int RalseiEyesClosedSmiling = 8;
        public const int RalseiConfused = 9;
        public const int RalseiAngry = 10;
        public const int RalseiNeutral = 11;
        public const int RalseiUnamused = 12;

        public const int LancerTongueOut = 0;
        public const int LancerSmileToothyTongueOut = 1;
        public const int LancerSmileToothy = 2;
        public const int LancerSmileTongueOut = 3;
        public const int LancerNervousFlag = 4;
        public const int LancerSad = 5;
        public const int LancerNeutralTongueOut = 6;
        public const int LancerSpeechlessTongueOut = 7;
        public const int LancerHandsome = 8;
        public const int LancerEvilTongueOut = 9;
        public const int LancerSadMouthOpenLookingDown = 10;
        public const int LancerSadLookingDown = 11;
        public const int LancerNervous = 12;
        public const int LancerDisguiseShock = 13;
        public const int LancerDisguiseSmile = 14;
    }
}
