﻿using Godot;

namespace TestGodotProject.classes
{
    /// <summary>
    /// Simple class to help with sleeping for async tasks
    /// </summary>
    class Sleeper
    {
        /// <summary>
        /// Delay execution of the current thread by given seconds
        /// </summary>
        /// <param name="seconds"></param>
        public static void DelaySeconds(float seconds)
        {
            int ms = (int)(seconds * 1000);
            OS.DelayMsec(ms);
        }
    }
}
