﻿namespace TestGodotProject.classes
{
    class DialogLine
    {
        public bool IsNewline = false;

        private readonly char[] _line;
        private int _fillIndex;

        /// <summary>
        /// Manages individual lines of a DialogText object
        /// </summary>
        /// <param name="length">The number of characters in the line</param>
        public DialogLine(int length = 24)
        {
            _line = new char[length];
        }

        /// <summary>
        /// Returns the line as a char[]
        /// </summary>
        public char[] GetLine()
        {
            return _line;
        }

        /// <summary>
        /// Attempts to write a character into the line
        /// </summary>
        public bool TryPutChar(char c)
        {
            if (!IsFull())
            {
                _line[_fillIndex] = c;
                _fillIndex++;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Attempts to fit a whole word string into the line. 
        /// If there isn't enough room, it fills the remaining space with blanks 
        /// </summary>
        public bool TryPutWholeWord(string word)
        {
            // Get number of free spaces left
            int roomLeft = (_line.Length) - _fillIndex;

            // If there is room left
            if (roomLeft - word.Length >= 0)
            {
                // Put the whole word into the array
                foreach (char c in word)
                {
                    _line[_fillIndex] = c;
                    _fillIndex++;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsEmpty()
        {
            return _fillIndex == 0;
        }

        public bool IsFull()
        {
            return _fillIndex == _line.Length;
        }
    }
}
